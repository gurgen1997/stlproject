#ifndef LStack_h__
#define LStack_h__

template <class T>
class LStack
{
private:
	struct Node
	{
		T m_value;
		Node* m_next;

		Node(T value = T(), Node* next = nullptr);
	};

private:
	Node* m_top;
	int m_size;

public:
	LStack();

	void push(const T& element);
	void pop();
	int size() const;
	T top() const;
};

// ################################
// struct Node functions
template <class T>
LStack<T>::Node::Node(T value = T(), Node* next = nullptr)
	: m_value(value)
	, m_next(next)
{
	
}

// ################################
// class LStack functions

template <class T>
LStack<T>::LStack()
	: m_top(nullptr)
	, m_size(0)
{

}

template <class T>
void LStack<T>::push(const T& element)
{
	Node* const newNode = new Node(element, m_top);
	m_top = newNode;

	++m_size;
}

template <class T>
void LStack<T>::pop()
{
	m_top = m_top->m_next;

	m_size--;
}

template <class T>
int LStack<T>::size() const
{
	return m_size;
}

template <class T>
T LStack<T>::top() const
{
	return m_top->m_value;
}

#endif // !LStack_h__