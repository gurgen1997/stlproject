#ifndef List_h__
#define List_h__

template <class T>
class List
{
private:
	struct Node
	{
		T m_value;
		Node* m_next;
		Node* m_previous;

		Node();
		Node(const T& value, Node* const previous, Node* const next);

	};

	class Iterator
	{
	private:
		Node* m_current;

	public:
		friend List;

		explicit Iterator(Node* const current);

		bool operator!=(const Iterator& rhs) const;
		bool operator==(const Iterator& rhs) const;

		typename List<T>::Iterator& operator++(int);
		typename List<T>::Iterator& operator++();
		typename List<T>::Iterator& operator--(int);
		typename List<T>::Iterator& operator--();

		int& operator*();
	};

	class Const_Iterator
	{
	private:
		const Node* m_current;

	public:
		friend List;

		explicit Const_Iterator(const Node* const current);

		bool operator!=(const Const_Iterator& rhs) const;
		bool operator==(const Const_Iterator& rhs) const;

		typename List<T>::Const_Iterator& operator++(int);
		typename List<T>::Const_Iterator& operator++();
		typename List<T>::Const_Iterator& operator--(int);
		typename List<T>::Const_Iterator& operator--();

		const int& operator*();
	};
private:
	void checkIsEmpty() const;
	void underflowException() const;

private:
	Node m_head;
	Node m_tail;
	int  m_size;

public:
	List();
	List(const List<T>& other);
	
	Const_Iterator cbegin() const;
	Const_Iterator cend() const;
	Iterator begin();
	Iterator end();
	Iterator insert(const Iterator& position, const T& element);
	Iterator erase(const Iterator& position);

	T front();
	T back();

	bool isEmpty() const;

	int size() const;

	void pushBack(const T& element);
	void pushFront(const T& element);
	void popBack();
	void popFront();

	List& operator=(const List<T>& other);

	~List();
};

// ######################################
// struct Node functions

template <class T>
List<T>::Node::Node()
{

}

template <class T> 
List<T>::Node::Node(const T& value, Node* const previous, Node* const next)
	: m_value(value)
	, m_previous(previous)
	, m_next(next)
{

}

// ######################################
// class Iterator functions
template<class T>
List<T>::Iterator::Iterator(Node* const current)
	: m_current(current)
{

}

template<class T>
bool List<T>::Iterator::operator!=(const Iterator& rhs) const
{
	return !(operator==(rhs));
}

template <class T>
bool List<T>::Iterator::operator==(const Iterator& rhs) const
{
	return rhs.m_current == m_current;
}

template <class T>
typename List<T>::Iterator& List<T>::Iterator::operator++(int)
{
	m_current = m_current->m_next;
	return *this;
}


template <class T>
typename List<T>::Iterator& List<T>::Iterator::operator++()
{
	m_current = m_current->m_next;
	return *this;
}

template <class T>
typename List<T>::Iterator& List<T>::Iterator::operator--(int)
{
	m_current = m_current->m_previous;
	return *this;
}

template <class T>
typename List<T>::Iterator& List<T>::Iterator::operator--()
{
	m_current = m_current->m_previous;
	return *this;
}

template <class T>
int& List<T>::Iterator::operator*()
{
	return m_current->m_value;
}

// ######################################
// class Const_Iterator functions
template<class T>
List<T>::Const_Iterator::Const_Iterator(const Node* const current)
	: m_current(current)
{

}

template<class T>
bool List<T>::Const_Iterator::operator!=(const Const_Iterator& rhs) const
{
	return !(operator==(rhs));
}

template <class T>
bool List<T>::Const_Iterator::operator==(const Const_Iterator& rhs) const
{
	return rhs.m_current == m_current;
}

template <class T>
typename List<T>::Const_Iterator& List<T>::Const_Iterator::operator++(int)
{
	m_current = m_current->m_next;
	return *this;
}


template <class T>
typename List<T>::Const_Iterator& List<T>::Const_Iterator::operator++()
{
	m_current = m_current->m_next;
	return *this;
}

template <class T>
typename List<T>::Const_Iterator& List<T>::Const_Iterator::operator--(int)
{
	m_current = m_current->m_previous;
	return *this;
}

template <class T>
typename List<T>::Const_Iterator& List<T>::Const_Iterator::operator--()
{
	m_current = m_current->m_previous;
	return *this;
}

template <class T>
const int& List<T>::Const_Iterator::operator*()
{
	return m_current->m_value;
}

// ######################################
// class List private functions
template <class T>
void List<T>::checkIsEmpty() const
{
	if (isEmpty())
	{
		underflowException();
	}
}

template <class T>
void List<T>::underflowException() const
{
	std::string exception = std::string("The stack is empty.");
	throw std::underflow_error(exception);
}

// ######################################
// class List public functions
template <class T>
List<T>::List()
	: m_size(0)
{
	m_head.m_next = &m_tail;
	m_tail.m_previous = &m_head;
}

template <class T>
List<T>::List(const List<T>& other)
	: List()
{
	for (auto it = other.cbegin(); it != other.cend(); ++it)
	{
		pushBack(*it);
	}
}

template <class T>
typename List<T>::Const_Iterator List<T>::cbegin() const
{
	return Const_Iterator(m_head.m_next);
}

template <class T>
typename List<T>::Const_Iterator List<T>::cend() const
{
	return Const_Iterator(&m_tail);
}

template <class T>
typename List<T>::Iterator List<T>::begin()
{
	return Iterator(m_head.m_next);
}

template <class T>
typename List<T>::Iterator List<T>::end()
{
	return Iterator(&m_tail);
}

template <class T>
typename List<T>::Iterator List<T>::insert(const Iterator& position, const T& element)
{
	Node* const next = position.m_current;
	Node* const previous = position.m_current->m_previous;

	Node* const newNode = new Node(element, previous, next);
	next->m_previous = newNode;
	previous->m_next = newNode;

	m_size++;

	return Iterator(newNode);
}

template <class T>
typename List<T>::Iterator List<T>::erase(const Iterator& position)
{
	checkIsEmpty();

	position.m_current->m_previous->m_next = position.m_current->m_next;
	position.m_current->m_next->m_previous = position.m_current->m_previous;

	Iterator tmp = position;

	delete position.m_current;

	m_size--;

	return Iterator(tmp.m_current->m_next);
}

template <class T>
T List<T>::front()
{
	return m_head.m_next->m_value;
}

template <class T>
T List<T>::back()
{
	return m_tail.m_previous->m_value;
}

template <class T>
bool List<T>::isEmpty() const
{
	return !m_size;
}

template <class T>
int List<T>::size() const
{
	return m_size;
}

template <class T>
void List<T>::pushBack(const T& element)
{
	Node* const next = &m_tail;
	Node* const previous = m_tail.m_previous;

	Node* newNode = new Node(element, previous, next);
	m_tail.m_previous->m_next = newNode;
	m_tail.m_previous = newNode;

	m_size++;
}


template <class T>
void List<T>::pushFront(const T& element)
{
	Node* const next = m_head.m_next;
	Node* const previous = &m_head;

	Node* newNode = new Node(element, previous, next);
	m_head.m_next->m_previous = newNode; 
	m_head.m_next = newNode;

	m_size++;
}

template <class T>
void List<T>::popBack()
{
	checkIsEmpty();

	m_tail.m_previous = m_tail.m_previous->m_previous;
	m_tail.m_previous->m_next = &m_tail;

	m_size--;
}

template <class T>
void List<T>::popFront()
{
	checkIsEmpty();

	m_head.m_next = m_head.m_next->m_next;
	m_head.m_next->m_previous = &m_head;

	m_size--;
}

template <class T>
typename List<T>::List& List<T>::operator=(const List<T>& other)
{
	while (m_size != 0)
	{
		popBack();
	}

	for (auto it = other.cbegin(); it != other.cend(); ++it)
	{
		pushBack(*it);
	}

	return *this;
}

template <class T>
List<T>::~List()
{
	while (m_size != 0)
	{
		popBack();
	}
}

#endif