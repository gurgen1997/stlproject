#ifndef Array_h__
#define Array_h__

#define SIZE 20

template <class T>
class Array
{
private:
	T m_data[SIZE];
	const int m_capacity;
	int m_size;

private:
	void overflowException() const;
	void underflowException() const;
	void checkIsFull() const;
	void checkIsEmpty() const;
	void checkRange(const int index) const;

public:
	typedef int* Iterator;

	Array();
	Array(std::initializer_list<T> arr);

	Iterator begin();
	Iterator end();
	bool isFull() const;
	bool isEmpty() const;
	int size() const;
	void pushBack(const T& element);
	void popBack();
	void insert(Iterator itr, const T& element);
	void erase(Iterator itr);

	const int& operator[](const int index) const;
	int& operator[](const int index); 

	~Array();
};

// #######################################
// private functions
template <class T>
void Array<T>::overflowException() const
{
	const std::string exception = std::string("The stack is full.");
	throw std::overflow_error(exception);
}

template <class T>
void Array<T>::underflowException() const
{
	const std::string exception = std::string("The stack is empty.");
	throw std::underflow_error(exception);
}

template <class T>
void  Array<T>::checkIsFull() const
{
	if (isFull())
	{
		overflowException();
	}
}

template <class T>
void Array<T>::checkIsEmpty() const
{
	if (isEmpty())
	{
		underflowException();
	}
}

template <class T>
void Array<T>::checkRange(const int index) const
{
	if (!(index >= 0 && index <= m_size))
	{
		const std::string exception = std::string("Index is out of range ") + std::to_string(index);
		throw std::out_of_range(exception);
	}
}

// #######################################
// public functions
template <class T>
Array<T>::Array()
	: m_capacity(SIZE)
	, m_size(0)
{

}

template <class T>
Array<T>::Array(std::initializer_list<T> arr)
	: Array()
{
	for (auto& element : arr)
	{
		pushBack(element);
	}
}

template <class T>
typename Array<T>::Iterator Array<T>::begin()
{
	return m_data;
}

template <class T>
typename Array<T>::Iterator Array<T>::end()
{
	return m_data + m_size;
}

template <class T>
bool Array<T>::isFull() const
{
	return m_size == m_capacity;
}

template <class T>
bool Array<T>::isEmpty() const
{
	return m_size == 0;
}

template <class T>
int Array<T>::size() const
{
	return m_size;
}

template <class T>
void Array<T>::pushBack(const T& element)
{
	checkIsFull();

	m_data[m_size] = element;
	m_size++;
}

template <class T>
void Array<T>::popBack() 
{
	checkIsEmpty();

	m_size--;
}

template <class T>
void  Array<T>::insert(Iterator itr, const T& element)
{
	checkIsFull();

	m_size++;
	for (auto p = end(); p != itr; p--)
	{
		*(p) = *(p - 1);
	}
	*(itr) = element;
}

template <class T>
void Array<T>::erase(Iterator itr)
{
	checkIsEmpty();

	for (auto p = itr; p != end(); p++)
	{
		*(p) = *(p + 1);
	}
	m_size--;
}

template <class T>
const int& Array<T>::operator[](const int index) const
{
	checkRange(index);
	return m_data[index];
}

template <class T>
int& Array<T>::operator[](const int index)
{
	checkRange(index);
	return m_data[index];
}

template <class T>
Array<T>::~Array()
{
	
}

#endif // ! Array_h__