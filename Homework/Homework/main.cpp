#include <iostream>
#include <conio.h>

#include "Vector.h"
#include "BST.h"

int main()
{
	Vector<int> vec;

	std::cout << "Size: " << vec.size() << std::endl;
	std::cout << "Capacity: " << vec.capacity() << std::endl;

	for (int i = 0; i < 20; i++)
	{
		vec.pushBack(i);
	}

	std::cout << "Size: " << vec.size() << std::endl;
	std::cout << "Capacity: " << vec.capacity() << std::endl;

	_getch();
	return 0;
}