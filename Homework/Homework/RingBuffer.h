#ifndef RingBuffer_h__
#define RingBuffer_h__

#include <assert.h>

template <class T>
class RingBuffer
{
private:
	int m_capacity;
	int m_size;
	T* m_data;

	int m_first;
	int m_last;

private:
	int getNextIndex(const int index) const;

public:
	RingBuffer(const int capacity);

	T front() const;
	T back() const;

	bool isEmpty() const;
	bool isFull() const;

	int  size() const;

	void push(const T& element);
	void pop();

	RingBuffer(const RingBuffer<T>& other) = delete;
	RingBuffer& operator=(const RingBuffer<T>& rhs) = delete;
	~RingBuffer();
};

// ########################################
// private functions
template <class T>
int RingBuffer<T>::getNextIndex(const int index) const
{
	return (index + 1) % m_capacity;
}

// ########################################
// public functions
template<class T>
RingBuffer<T>::RingBuffer(const int capacity)
	: m_capacity(capacity)
	, m_size(0)
	, m_data(new T[m_capacity])
	, m_first(0)
	, m_last(0)
{

}

template <class T>
T RingBuffer<T>::front() const
{
	const auto NextToFirst = getNextIndex(m_first);
	return m_data[NextToFirst];
}

template <class T>
T RingBuffer<T>::back() const
{
	return m_data[m_last];
}

template <class T>
bool RingBuffer<T>::isEmpty() const
{
	return m_size == 0;
}

template <class T>
bool RingBuffer<T>::isFull() const
{
	return m_size == m_capacity;
}

template <class T>
int RingBuffer<T>::size() const
{
	return m_size;
}

template <class T>
void RingBuffer<T>::push(const T& element)
{
	assert(!isFull() && "The stack is full.");

	m_last = getNextIndex(m_last);
	m_data[m_last] = element;
	++m_size;
}

template <class T>
void RingBuffer<T>::pop()
{
	// chechIsEmpty();
	assert(!isEmpty() && "The stack is empty.");

	m_first = getNextIndex(m_first);
	--m_size;
}

template <class T>
RingBuffer<T>::~RingBuffer()
{
	delete[] m_data;
}

#endif 
