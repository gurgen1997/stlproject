#ifndef Vector_h__
#define Vector_h__

template <class T>
class Vector
{
public:
	typedef int* Iterator;

private:
	static void copyElement(const T* const from, T* const to, int count);

	bool isReallocationNecessaryForMoreMemory();
	bool isReallocationNecessaryForLessMemory();

	void reallocateMoreMemory();
	void reallocateLessMemory();
	void reallocateMemory();
	void insertInMemory(Iterator it, const T& element);
	void eraseInMemory(Iterator it);

	void checkRange(const int index) const;
	void overFlowException() const;
	void undeFlowException() const;
	void checkIsEmpty() const;
	void checkIsFull() const;

public:
	Vector();
	Vector(const Vector<T>& other);
	Vector(std::initializer_list<T> vec);

	Iterator begin();
	Iterator end();

	bool isEmpty() const;
    bool isFull() const;

	int size() const;

	void pushBack(const T& element);
	void popBack();
	void insert(Iterator it, const T& element);
	void erase(Iterator it);

	int capacity() const
	{
		return m_capacity;
	}

	Vector<T>& operator=(const Vector<T>& other);
	int& operator[](const int index);
	const int& operator[](const int index) const;

	~Vector();

private:
	int m_capacity;
	int m_size;
	T* m_data;
};

// ###########################################
// private function
template <class T>
void Vector<T>::copyElement(const T* const from, T* const to, int count)
{
	for (int i = 0; i < count; i++)
	{
		to[i] = from[i];
	}
}

template <class T>
bool Vector<T>::isReallocationNecessaryForMoreMemory()
{
	return m_size == m_capacity;
}

template <class T>
bool Vector<T>::isReallocationNecessaryForLessMemory()
{
	return m_size < m_capacity / 2;
}

template <class T>
void  Vector<T>::reallocateMoreMemory()
{
	m_capacity *= 2;

	reallocateMemory();
}

template <class T>
void Vector<T>::reallocateLessMemory()
{
	m_capacity /= 2;

	reallocateMemory();
}

template <class T>
void Vector<T>::reallocateMemory()
{
	T* const tmp = new T[m_capacity];
	copyElement(m_data, tmp, m_size);

	delete[] m_data;

	m_data = tmp;
}

template <class T>
void Vector<T>::insertInMemory(Iterator it, const T& element)
{
	m_size++;
	for (auto p = end(); p != it; p--)
	{
		*(p) = *(p - 1);
	}
	*(it) = element;
}

template <class T>
void Vector<T>::eraseInMemory(Iterator it)
{
	for (auto p = it; p != end(); p++)
	{
		*(p) = *(p + 1);
	}
	m_size--;
}

template <class T>
void Vector<T>::checkRange(const int index) const
{
	if (!(index >= 0 && index <= m_size))
	{
		const std::string exception = std::string("Index is out of range ") + std::to_string(index);
		throw std::out_of_range(exception);
	}
}

template <class T>
void Vector<T>::overFlowException() const
{
	const std::string exception("The stack is full.");
	throw std::overflow_error(exception);
}

template <class T>
void Vector<T>::undeFlowException() const
{
	const std::string exception("The stack is empty");
	throw std::underflow_error(exception);
}

template <class T>
void Vector<T>::checkIsEmpty() const
{
	if (isEmpty())
	{
		undeFlowException();
	}
}

template <class T>
void Vector<T>::checkIsFull() const
{
	if (isFull())
	{
		overFlowException();
	}
}

// ###########################################
// public function
template <class T>
Vector<T>::Vector()
	: m_capacity(20)
	, m_size(0)
	, m_data(new T[m_capacity])
{
	
}

template <class T>
Vector<T>::Vector(const Vector<T>& other)
	: m_capacity(other.m_capacity)
	, m_size(other.m_size)
	, m_data(new T[m_capacity])
{
	copyElement(other.m_data, m_data, m_size);
}

template <class T>
Vector<T>::Vector(std::initializer_list<T> vec)
	: Vector()
{
	for (auto& element : vec)
	{
		pushBack(element);
	}
}

template <class T>
typename Vector<T>::Iterator Vector<T>::begin()
{
	return m_data;
}

template <class T>
typename Vector<T>::Iterator Vector<T>::end()
{
	return m_data + m_size;
}

template <class T>
bool  Vector<T>::isEmpty() const
{
	return m_size == 0;
}

template <class T>
bool  Vector<T>::isFull() const
{
	return m_size == m_capacity;
}

template <class T>
int Vector<T>::size() const
{
	return m_size;
}

template <class T>
void Vector<T>::pushBack(const T& element)
{
	insert(end(), element);
}

template <class T>
void Vector<T>::popBack()
{
	erase(end());
}

template <class T>
void Vector<T>::insert(Iterator it, const T& element)
{
	if (isReallocationNecessaryForMoreMemory())
	{
		reallocateMoreMemory();
	}
	insertInMemory(it, element);
}

template <class T>
void Vector<T>::erase(Iterator it)
{
	if (isReallocationNecessaryForLessMemory())
	{
		reallocateLessMemory();
	}
	eraseInMemory(it);
}

template <class T>
Vector<T>& Vector<T>::operator=(const Vector<T>& other)
{
	T* const tmp = new T[other.m_capacity];
	copyElement(other.m_data, tmp, other.m_size);

	delete[] m_data;

	m_capacity = other.m_capacity;
	m_size = other.m_size;
	m_data = tmp;

	return *this;
}

template <class T>
int& Vector<T>::operator[](const int index)
{
	checkRange(index);
	return m_data[index];
}

template <class T>
const int& Vector<T>::operator[](const int index) const
{
	checkRange(index);
	return m_data[index];
}

template <class T>
Vector<T>::~Vector()
{
	delete m_data;
}

#endif