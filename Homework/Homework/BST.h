#ifndef BST_h__
#define BST_h__

#include <queue>
#include <stack>

template <class T>
class BST
{
private:
	struct Node
	{
		T m_value;
		Node* m_left;
		Node* m_right;

		Node(const T& value = T(), Node* const left = nullptr, Node* const right = nullptr);
	};

private:
	static void clean(Node* leaf);

	bool hasLeft(const Node* const leaf);
	bool hasRight(const Node* const leaf);

	void insertPrivate(const T& value, Node* leaf);
	void printInOrderPrivate(Node* leaf);

	Node* findPrivate(const T& value, Node* leaf);
	Node* minimumPrivate(Node* leaf);
	Node* maximumPriavet(Node* leaf);
	Node* deleteNodePrivate(const T& value, Node* leaf);
	Node* lcaPrivate(Node* root, const T& value1, const T& value2);

private:
	Node* m_root;

public:
	BST();
	BST(std::initializer_list<T> list);

	bool isEmpty() const;

	void insert(const T& value);
	void printInOrder();
	void bfsTraverse();
	void dfsTraverse();

	Node* find(const T& value);
	Node* minimum();
	Node* maximum();
	Node* successor(Node* toFind);
	Node* predecessor(Node* toFind);
	Node* deleteNode(const T& value);
	Node* lca(const T& value1, const T& value2);

	BST(const BST& other) = delete;
	BST& operator=(const BST& other) = delete;

	~BST();
};

// ###########################
// struct Node functions

template <class T>
BST<T>::Node::Node(const T& value, Node* const left, Node* const right)
	: m_value(value)
	, m_left(left)
	, m_right(right)
{

}

// ###########################
// class BST private functions

template <class T>
void BST<T>::clean(Node* leaf)
{
	if (leaf != nullptr)
	{
		clean(leaf->m_left);
		clean(leaf->m_right);

		delete leaf;
	}
}

template <class T>
bool BST<T>::hasLeft(const Node* const leaf)
{
	return leaf->m_left != nullptr;
}

template <class T>
bool BST<T>::hasRight(const Node* const leaf)
{
	return leaf->m_right;
}

template <class T>
void BST<T>::insertPrivate(const T& value, Node* leaf)
{
	if (value <= leaf->m_value)
	{
		if (leaf->m_left != nullptr)
		{
			insertPrivate(value, leaf->m_left);
		}
		else
		{
			leaf->m_left = new Node(value, nullptr, nullptr);
		}
	}
	else
	{
		if (leaf->m_right != nullptr)
		{
			insertPrivate(value, leaf->m_right);
		}
		else
		{
			leaf->m_right = new Node(value, nullptr, nullptr);
		}
	}
}

template <class T>
void BST<T>::printInOrderPrivate(Node* leaf)
{
	if (m_root != nullptr)
	{
		if (leaf->m_left != nullptr)
		{
			printInOrderPrivate(leaf->m_left);
		}

		std::cout << leaf->m_value << " ";

		if (leaf->m_right != nullptr)
		{
			printInOrderPrivate(leaf->m_right);
		}
	}
}

template <class T>
typename BST<T>::Node* BST<T>::findPrivate(const T& value, Node* leaf)
{
	if (leaf != nullptr)
	{
		if (value == leaf->m_value)
		{
			return leaf;
		}
		else
		{
			if (value <= leaf->m_value)
			{
				return findPrivate(value, leaf->m_left);
			}
			else
			{
				return findPrivate(value, leaf->m_right);
			}
		}
	}
	else
	{
		return nullptr;
	}
}

template <class T>
typename BST<T>::Node* BST<T>::minimumPrivate(Node* leaf)
{
	if (leaf == nullptr)
	{
		return nullptr;
	}
	else
	{
		Node* tmp = leaf;

		if (hasLeft(tmp))
		{
			while (hasLeft(tmp))
			{
				tmp = tmp->m_left;
			}

			return tmp;
		}
		else
		{
			return tmp;
		}
	}
}

template <class T>
typename BST<T>::Node* BST<T>::maximumPriavet(Node* leaf)
{
	if (leaf == nullptr)
	{
		return nullptr;
	}
	else
	{
		Node* tmp = leaf;

		if (hasRight(tmp))
		{
			while (hasRight(tmp))
			{
				tmp = tmp->m_right;
			}

			return tmp;
		}
		else
		{
			return tmp;
		}
	}
}

template <class T>
typename BST<T>::Node* BST<T>::deleteNodePrivate(const T& value, Node* leaf)
{
	if (leaf == nullptr)
	{
		return leaf;
	}
	else if (value < leaf->m_value)
	{
		leaf->m_left = deleteNodePrivate(value, leaf->m_left);
	}
	else if (value > leaf->m_value)
	{
		leaf->m_right = deleteNodePrivate(value, leaf->m_right);
	}
	else
	{
		if (leaf->m_left == nullptr && leaf->m_right == nullptr)
		{
			delete leaf;
			leaf = nullptr;
		}
		else if (leaf->m_left == nullptr)
		{
			Node* temp = leaf;
			leaf = leaf->m_right;

			delete temp;
		}
		else if (leaf->m_right == nullptr)
		{
			Node* temp = leaf;
			leaf = leaf->m_left;

			delete temp;
		}
		else
		{
			Node* temp = minimumPrivate(leaf->m_right);

			leaf->m_value = temp->m_value;
			leaf->m_right = deleteNodePrivate(temp->m_value, leaf->m_right);
		}
	}

	return leaf;
}

template <class T>
typename BST<T>::Node* BST<T>::lcaPrivate(Node* root, const T& value1, const T& value2)
{
	if (root == nullptr)
	{
		return nullptr;
	}

	if (root->m_value > value1 && root->m_value > value2)
	{
		return lcaHelper(root->m_left, value1, value2);
	}
	if (root->m_value < value1 && root->m_value < value2)
	{
		return lcaHelper(root->m_right, value1, value2);
	}

	return root;
}

// ###########################
// class BST public functions

template <class T>
BST<T>::BST()
	: m_root(nullptr)
{
	
}

template <class T>
BST<T>::BST(std::initializer_list<T> list)
{
	for (auto& element : list)
	{
		insert(element);
	}
}

template <class T>
bool BST<T>::isEmpty() const
{
	return m_root == nullptr;
}

template <class T>
void BST<T>::insert(const T& value)
{
	if (m_root == nullptr)
	{
		m_root = new Node(value, nullptr, nullptr);
	}
	else
	{
		insertPrivate(value, m_root);
	}
}

template <class T>
void BST<T>::printInOrder()
{
	printInOrderPrivate(m_root);
}

template <class T>
void BST<T>::bfsTraverse()
{
	std::queue<Node *> pendingNodes;
	pendingNodes.push(m_root);

	while (pendingNodes.size() != 0)
	{
		Node* current = pendingNodes.front();
		pendingNodes.pop();

		if (hasLeft(current))
		{
			pendingNodes.push(current->m_left);
		}

		if (hasRight(current))
		{
			pendingNodes.push(current->m_right);
		}

		std::cout << current->m_value << " ";
	}
}

template <class T>
void BST<T>::dfsTraverse()
{
	std::stack<Node*> pendingNodes;

	pendingNodes.push(m_root);

	while (pendingNodes.size() > 0)
	{
		Node* current = pendingNodes.top();

		std::cout << current->m_value<< " ";

		pendingNodes.pop();

		if (hasLeft(current))
		{
			pendingNodes.push(current->m_left);
		}

		if (hasRight(current))
		{
			pendingNodes.push(current->m_right);
		}
	}
}

template <class T>
typename BST<T>::Node* BST<T>::find(const T& value)
{
	return findPrivate(value, m_root);
}

template <class T>
typename BST<T>::Node* BST<T>::minimum()
{
	return minimumPrivate(m_root);
}

template <class T>
typename BST<T>::Node* BST<T>::maximum()
{
	return maximumPriavet(m_root);
}

template <class T>
typename BST<T>::Node* BST<T>::successor(Node* toFind)
{
	if (m_root == nullptr)
	{
		return nullptr;
	}
	
	Node* successor = nullptr;
	Node* root = m_root;

	if (toFind->m_right != nullptr)
	{
		successor = toFind->m_right;

		while (successor->m_left)
		{
			successor = successor->m_left;
		}
	}
	else
	{
		while (root)
		{
			if (toFind->m_value < root->m_value)
			{
				successor = root;
				root = root->m_left;
			}
			else
			{
				root = root->m_right;
			}
		}
	}

	return successor;
}

template <class T>
typename BST<T>::Node* BST<T>::predecessor(Node* toFind)
{
	if (m_root == nullptr)
	{
		return nullptr;
	}
	
	Node* predecessor = nullptr;
	Node* root = m_root;

	if (toFind->m_left != nullptr)
	{
		predecessor = toFind->m_left;

		while (predecessor->m_right)
		{
			predecessor = predecessor->m_right;
		}
	}
	else
	{
		while (root)
		{
			if (toFind->m_value > root->m_value)
			{
				predecessor = root;

				root = root->m_right;
			}
			else
			{
				root = root->m_right;
			}
		}
	}

	return predecessor;
}

template <class T>
typename BST<T>::Node* BST<T>::deleteNode(const T& value)
{
	return deleteNodePrivate(value, m_root);
}

template <class T>
typename BST<T>::Node* BST<T>::lca(const T& value1, const T& value2)
{
	return lcaPrivate(m_root, value1, value2);
}

template <class T>
BST<T>::~BST()
{
	clean(m_root);
}

#endif