#include "stdafx.h"
#include "CppUnitTest.h"
#include "Array.h"
#include "Vector.h"
#include "List.h"
#include "LStack.h"
#include "RingBuffer.h"

#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(ArrayUnitTest)
	{
	private:
		template <class T>
		static std::string toString(const Array<T>& ls)
		{
			std::string result = "";

			for (int i = 0; i < ls.size(); i++)
			{
				const int value = ls[i];
				result += std::to_string(value);
				result += " ";
			}

			return result;
		}

	public:

		TEST_METHOD(CTOR_test1)
		{
			Array<int> arr;

			Assert::IsTrue(true, L"", LINE_INFO());
		}

		TEST_METHOD(CTOR_test2)
		{
			Array<int> arr{ 10, 20, 30, 40, 50 };

			const std::string actual = toString(arr);
			const std::string expected = "10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test1)
		{
			Array<int> arr;

			const int actual = arr.size();
			const int expectd = 0;
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test2)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);

			const int actual = arr.size();
			const int expectd = 2;
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test3)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			arr.popBack();

			const int actual = arr.size();
			const int expectd = 2;
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushBack_test1)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			const int actual = arr.size();
			const int expectd = 3;
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushBack_test2)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			const std::string actual = toString(arr);
			const std::string expectd = "10 20 30 ";
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushBack_test3)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			arr.popBack();

			const std::string actual = toString(arr);
			const std::string expectd = "10 20 ";
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushBack_test4)
		{
			Array<int> arr;

			for (int i = 0; i < 20; i++)
			{
				arr.pushBack(i);
			}

			try
			{
				arr.pushBack(200);
				Assert::Fail(L"Should fail");
			}
			catch (std::overflow_error& e)
			{
				const std::string expected("The stack is full.");
				const std::string actual(e.what());
				Assert::AreEqual(expected, actual, L"", LINE_INFO());
			}
		}

		TEST_METHOD(pushBack_test5)
		{
			Array<int> arr;

			for (int i = 0; i < 20; i++)
			{
				arr.pushBack(i);
			}

			auto func = [&]()
			{
				arr.pushBack(200);
			};

			Assert::ExpectException<std::overflow_error>(func);
		}

		TEST_METHOD(popBack_test1)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			arr.popBack();

			const int actual = arr.size();
			const int expectd = 2;
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(popBack_test2)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			arr.popBack();

			const std::string actual = toString(arr);
			const std::string expectd = "10 20 ";
			Assert::AreEqual(expectd, actual, L"", LINE_INFO());
		}

		TEST_METHOD(popBack_test3)
		{
			Array<int> arr;

			try
			{
				arr.popBack();
			}
			catch (std::underflow_error& e)
			{
				const std::string expected("The stack is empty.");
				const std::string actual(e.what());
				Assert::AreEqual(expected, actual, L"", LINE_INFO());
			}
		}

		TEST_METHOD(begin_test1)
		{
			Array<int> arr;

			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);

			Array<int>::Iterator it = arr.begin();

			const int actual = arr[0];
			const int expected = 10;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(begin_test2)
		{
			Array<int> arr;

			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.begin() + 2;

			const int actual = arr[2];
			const int expected = 30;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(end_test1)
		{
			Array<int> arr;

			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.end() - 1;
			const int actual = arr[4];
			const int expected = 50;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(insert_test1)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.begin() + 2;
			arr.insert(it, 200);

			const int actual = arr[2];
			const int expected = 200;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(insert_test2)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.begin() + 2;
			arr.insert(it, 200);

			const std::string actual = toString(arr);
			const std::string expected = "10 20 200 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(insert_test3)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.end() - 1;
			arr.insert(it, 200);

			const std::string actual = toString(arr);
			const std::string expected = "10 20 30 40 200 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(insert_test4)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.begin();
			arr.insert(it, 200);

			const std::string actual = toString(arr);
			const std::string expected = "200 10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(insert_test5)
		{
			Array<int> arr;
			for (int i = 0; i < 20; i++)
			{
				arr.pushBack(i);
			}

			try
			{
				arr.pushBack(200);

			}
			catch (std::overflow_error& e)
			{
				const std::string expected("The stack is full.");
				const std::string actual(e.what());
				Assert::AreEqual(expected, actual, L"", LINE_INFO());
			}
		}

		TEST_METHOD(erase_test1)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.begin();
			arr.erase(it);

			const std::string actual = toString(arr);
			const std::string expected = "20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(erase_test2)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.end() - 1;
			arr.erase(it);

			const std::string actual = toString(arr);
			const std::string expected = "10 20 30 40 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(erase_test3)
		{
			Array<int> arr;
			arr.pushBack(10);
			arr.pushBack(20);
			arr.pushBack(30);
			arr.pushBack(40);
			arr.pushBack(50);

			Array<int>::Iterator it = arr.begin() + 2;
			arr.erase(it);

			const std::string actual = toString(arr);
			const std::string expected = "10 20 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(erase_test4)
		{
			Array<int> arr;

			try
			{
				arr.popBack();
			}
			catch (std::underflow_error& e)
			{
				const std::string expected("The stack is empty.");
				const std::string actual(e.what());
				Assert::AreEqual(expected, actual, L"", LINE_INFO());
			}
		}

		TEST_METHOD(isEmpty_test1)
		{
			Array<int> arr;

			Assert::IsTrue(arr.isEmpty(), L"", LINE_INFO());
		}

		TEST_METHOD(isEmpty_test2)
		{
			Array<int> arr;

			arr.pushBack(10);

			Assert::IsFalse(arr.isEmpty(), L"", LINE_INFO());
		}

		TEST_METHOD(outOfRnage)
		{
			Array<int> arr;

			try
			{
				arr[1000];
			}
			catch (std::out_of_range& e)
			{
				const std::string expected("Index is out of range 1000");
				const std::string actual(e.what());
				Assert::AreEqual(expected, actual, L"", LINE_INFO());
			}
		}
	};

	TEST_CLASS(VectorUnitTest)
	{
	private:
		template <class T>
		static std::string toString(const Vector<T>& ls)
		{
			std::string result = "";

			for (int i = 0; i < ls.size(); i++)
			{
				const int value = ls[i];
				result += std::to_string(value);
				result += " ";
			}

			return result;
		}

	public:
		TEST_METHOD(CTOR_test1)
		{
			Vector<int> vec;
			Assert::IsTrue(true, L"", LINE_INFO());
		}

		TEST_METHOD(CTOR_test2)
		{
			Vector<int> vec{ 10, 20, 30, 40, 50 };

			const std::string actual = toString(vec);
			const std::string expected = "10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(copy_CTOR_test1)
		{
			Vector<int> vec1{ 10, 20, 30, 40, 50 };

			Vector<int> vec2(vec1);

			const std::string actual = toString(vec2);
			const std::string expected = "10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(copy_CTOR_test2)
		{
			Vector<int> vec1{ 10, 20, 30, 40, 50 };

			Vector<int> vec2(vec1);

			vec1.popBack();
			vec1.popBack();

			const std::string actual = toString(vec2);
			const std::string expected = "10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(copy_CTOR_test3)
		{
			Vector<int> vec1{ 10, 20, 30, 40, 50 };

			Vector<int> vec2(vec1);

			vec1.popBack();
			vec1.popBack();

			const std::string actual = toString(vec1);
			const std::string expected = "10 20 30 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(operatorAssignment_test1)
		{
			Vector<int> vec1{ 10, 20, 30, 40, 50 };

			Vector<int> vec2;
			vec2 = vec1;

			const std::string actual = toString(vec2);
			const std::string expected = "10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(operatorAssignment_test2)
		{
			Vector<int> vec1{ 10, 20, 30, 40, 50 };

			Vector<int> vec2;
			vec2 = vec1;

			vec1.popBack();
			vec1.popBack();

			const std::string actual = toString(vec2);
			const std::string expected = "10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(operatorAssignment_test3)
		{
			Vector<int> vec1{ 10, 20, 30, 40, 50 };

			Vector<int> vec2;
			vec2 = vec1;

			vec1.popBack();
			vec1.popBack();

			const std::string actual = toString(vec1);
			const std::string expected = "10 20 30 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test1)
		{
			Vector<int> vec;

			const int expected = 0;
			const int actual = vec.size();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test2)
		{
			Vector<int> vec;

			vec.pushBack(10);

			const int expected = 1;
			const int actual = vec.size();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(begin_test1)
		{
			Vector<int> vec;

			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Vector<int>::Iterator it = vec.begin();

			const int actual = 10;
			const int expected = vec[0];
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(begin_test2)
		{
			Vector<int> vec;

			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Vector<int>::Iterator it = vec.begin() + 2;

			const int actual = 30;
			const int expected = vec[2];
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(end_test1)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);

			Vector<int>::Iterator it = vec.end() - 1;

			const int actual = 40;
			const int expected = vec[3];
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(operatorSubscrpting_test1)
		{
			Vector<int> vec;

			try
			{
				vec[100];
			}
			catch (std::out_of_range& e)
			{
				const std::string expected("Index is out of range 100");
				const std::string actual(e.what());
				Assert::AreEqual(expected, actual, L"", LINE_INFO());
			}
		}

		TEST_METHOD(insert_test1)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.begin() + 2;
			vec.insert(it, 200);

			const int actual = vec[2];
			const int expected = 200;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(insert_test2)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.begin() + 2;
			vec.insert(it, 200);

			const std::string actual = toString(vec);
			const std::string expected = "10 20 200 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}


		TEST_METHOD(insert_test3)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.begin();
			vec.insert(it, 200);

			const std::string actual = toString(vec);
			const std::string expected = "200 10 20 30 40 50 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}


		TEST_METHOD(insert_test4)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.end();
			vec.insert(it, 200);

			const std::string actual = toString(vec);
			const std::string expected = "10 20 30 40 50 200 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(erase_test1)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.begin() + 2;
			vec.erase(it);

			const int actual = vec[2];
			const int expected = 40;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}


		TEST_METHOD(erase_test2)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.begin();
			vec.erase(it);

			const int actual = vec[0];
			const int expected = 20;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(erase_test3)
		{
			Vector<int> vec;
			vec.pushBack(10);
			vec.pushBack(20);
			vec.pushBack(30);
			vec.pushBack(40);
			vec.pushBack(50);

			Array<int>::Iterator it = vec.end();
			vec.erase(it);

			const std::string actual = toString(vec);
			const std::string expected = "10 20 30 40 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
	};

	TEST_CLASS(ListUnitTest)
	{
	private:
		template <class T>
		static std::string toString(List<T>& ls)
		{
			std::string result = "";

			for (auto it = ls.begin(); it != ls.end(); ++it)
			{
				result += std::to_string(*it);
				result += " ";
			}

			return result;
		}

	public:
		TEST_METHOD(CTOR_test1)
		{
			List<int> ls;

			Assert::IsTrue(true, L"", LINE_INFO());
		}

		TEST_METHOD(copy_CTOR_test1)
		{
			List<int> ls1;
			ls1.pushBack(10);
			ls1.pushBack(20);
			ls1.pushBack(30);

			List<int> ls2(ls1);
			const std::string actual = toString(ls2);
			const std::string expected = "10 20 30 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(copy_CTOR_test2)
		{
			List<int> ls1;
			ls1.pushBack(10);
			ls1.pushBack(20);
			ls1.pushBack(30);

			List<int> ls2(ls1);

			ls1.popBack();

			const std::string actual = toString(ls2);
			const std::string expected = "10 20 30 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushBack_test1)
		{
			List<int> ls;

			ls.pushBack(10);
			ls.pushBack(20);
			ls.pushBack(30);

			const int actual = ls.front();
			const int expected = 10;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushBack_test2)
		{
			List<int> ls;

			ls.pushBack(10);
			ls.pushBack(20);
			ls.pushBack(30);

			const int actual = ls.back();
			const int expected = 30;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(popBack_test1)
		{
			List<int> ls;

			ls.pushBack(10);
			ls.pushBack(20);
			ls.pushBack(30);

			ls.popBack();
			const int actual = ls.back();
			const int expected = 20;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pushFront_test1)
		{
			List<int> ls;

			ls.pushFront(10);
			ls.pushFront(20);
			ls.pushFront(30);

			const int actual = ls.front();
			const int expected = 30;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(popFront_test1)
		{
			List<int> ls;

			ls.pushFront(10);
			ls.pushFront(20);
			ls.pushFront(30);

			ls.popFront();

			const int actual = ls.front();
			const int expected = 20;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(popFront_test2)
		{
			List<int> ls;

			ls.pushFront(10);
			ls.pushFront(20);
			ls.pushFront(30);

			const std::string actual = toString(ls);
			const std::string expected = "30 20 10 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(isEmpty_test1)
		{
			List<int> ls;

			Assert::IsTrue(ls.isEmpty(), L"", LINE_INFO());
		}

		TEST_METHOD(isEmpty_test2)
		{
			List<int> ls;

			ls.pushFront(10);
			ls.pushFront(20);
			ls.pushFront(30);

			Assert::IsTrue(!ls.isEmpty(), L"", LINE_INFO());
		}

		TEST_METHOD(operatorAssignment_test1)
		{
			List<int> ls1;
			ls1.pushBack(10);
			ls1.pushBack(20);
			ls1.pushBack(30);

			List<int> ls2;
			ls2 = ls1;

			ls1.popBack();

			const std::string actual = toString(ls2);
			const std::string expected = "10 20 30 ";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
	};

	TEST_CLASS(RingBufferUnitTest)
	{
	public:
		TEST_METHOD(CTOR_test1)
		{
			RingBuffer<int> ring(10);

			Assert::IsTrue(true, L"", LINE_INFO());
		}

		TEST_METHOD(CTOR_test2)
		{
			RingBuffer<double> ring(10);

			Assert::IsTrue(true, L"", LINE_INFO());
		}

		TEST_METHOD(CTOR_test3)
		{
			RingBuffer<std::string> ring(10);

			Assert::IsTrue(true, L"", LINE_INFO());
		}

		TEST_METHOD(size_test1)
		{
			RingBuffer<int> ring(5);

			const int actual = ring.size();
			const int expected = 0;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test2)
		{
			RingBuffer<int> ring(5);
			ring.push(10);
			ring.push(20);
			ring.push(30);

			const int actual = ring.size();
			const int expected = 3;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test3)
		{
			RingBuffer<std::string> ring(5);
			ring.push("Hello");
			ring.push("World");

			const int actual = ring.size();
			const int expected = 2;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(size_test4)
		{
			RingBuffer<double> ring(5);
			ring.push(1.1);
			ring.push(2.2);
			ring.push(3.3);

			ring.pop();

			const int actual = ring.size();
			const int expected = 2;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(push_test1)
		{
			RingBuffer<int> ring(5);
			ring.push(10);
			ring.push(20);
			ring.push(30);
			ring.push(40);
			ring.push(50);

			const int actual = ring.size();
			const int expected = 5;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(push_test2)
		{
			RingBuffer<std::string> ring(5);
			ring.push("vector");
			ring.push("list");
			ring.push("deque");

			const int actual = ring.size();
			const int expected = 3;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pop_test1)
		{
			RingBuffer<int> ring(10);
			ring.push(10);
			ring.push(20);
			ring.push(30);
			ring.push(40);
			ring.push(50);

			ring.pop();
			ring.pop();

			const int actual = ring.size();
			const int expected = 3;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(pop_test2)
		{
			RingBuffer<int> ring(10);

			// the following line of code will cause 
			// the program stop working
			// "The stack is empty."
			// ring.pop();

			const int actual = ring.size();
			const int expected = 0;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(front_test1)
		{
			RingBuffer<int> ring(5);
			ring.push(10);
			ring.push(20);
			ring.push(30);

			const int actual = ring.front();
			const int expected = 10;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(front_test2)
		{
			RingBuffer<std::string> ring(5);
			ring.push("vector");
			ring.push("list");
			ring.push("deque");

			const std::string actual = ring.front();
			const std::string expected = "vector";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(front_test3)
		{
			RingBuffer<int> ring(5);
			ring.push(10);
			ring.push(20);
			ring.push(30);

			ring.pop();

			const int actual = ring.front();
			const int expected = 20;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(front_test4)
		{
			RingBuffer<std::string> ring(5);
			ring.push("vector");
			ring.push("list");
			ring.push("deque");

			ring.pop();

			const std::string actual = ring.front();
			const std::string expected = "list";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(back_test1)
		{
			RingBuffer<int> ring(5);
			ring.push(10);
			ring.push(20);
			ring.push(30);

			const int actual = ring.back();
			const int expected = 30;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(back_test2)
		{
			RingBuffer<std::string> ring(5);
			ring.push("Hello");
			ring.push("World");

			const std::string actual = ring.back();
			const std::string expected = "World";
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(isFull_test1)
		{
			RingBuffer<double> ring(3);
			ring.push(1.1);
			ring.push(2.2);
			ring.push(3.3);

			// this line of code will cause the program 
			// to stop working
			// "the stack is full"
			// ring.push(4.4);

			Assert::IsTrue(ring.isFull(), L"", LINE_INFO());
		}

		TEST_METHOD(isFull_test2)
		{
			RingBuffer<int> ring(5);
			ring.push(1);
			ring.push(2);
			ring.push(3);

			const bool actual = ring.isFull();
			const bool expected = false;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(isEmpty_test1)
		{
			RingBuffer <std::string> ring(3);

			const bool actual = ring.isEmpty();
			const bool expected = true;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

		TEST_METHOD(isEmpty_test2)
		{
			RingBuffer<double> ring(5);
			ring.push(1.1);
			ring.push(2.2);
			ring.push(3.3);

			const bool actual = ring.isEmpty();
			const bool expected = false;
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
	};
}